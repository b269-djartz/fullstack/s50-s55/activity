import { Button, Row, Col } from "react-bootstrap"
import {Link, NavLink} from "react-router-dom"

export default function Banner(banner) {
   const {title,description,buttonText,buttonLink} = banner
  return (
   
    <Row>
      <Col className="p-5">
        <h1>{title}</h1>
        <p>{description}</p>
        {buttonText && (
          <Button variant="primary" as={Link} to={buttonLink}>{buttonText}</Button>
        )}
      </Col>
    </Row>
  );
}
